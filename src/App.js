import React from 'react';
import './App.css';
import { Box, Code, Container, Button, Heading, Input, InputGroup, InputRightElement, VStack, Text, UnorderedList, ListItem, Link } from "@chakra-ui/react"

class App extends React.Component {

  state = {
    tasks: [
      "Learn how to work with arrays",
      "Prepare a portfolio website",
    ],
    newTask: "",
  }

  onChangeNewTask(event) {
    const newTaskText = event.target.value
    this.setState({newTask: newTaskText})
  }

  addTask() {
    let tasks = this.state.tasks
    tasks.push(this.state.newTask)
    this.setState({tasks: tasks, newTask: ""})
  }

  render() {

    return (
      <Container>
        <Heading>Todo list</Heading>
        <UnorderedList>
          {this.state.tasks.map(task => {
            return <ListItem>{task}</ListItem>
          })}
        </UnorderedList>
        <InputGroup size="md">
          <Input placeholder="Task content" value={this.state.newTask} onChange={this.onChangeNewTask.bind(this)} />
          <InputRightElement>
            <Button h="1.75rem" size="sm" onClick={this.addTask.bind(this)}>
              Add
            </Button>
          </InputRightElement>
        </InputGroup>
      </Container>
    );
  }
}

export default App;
